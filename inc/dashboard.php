<?php
/**
 * Created by PhpStorm.
 * User: 77711
 * Date: 28-1-2016
 * Time: 11:30
 */


/**
 * GET "users"
 * Van dashboard naar users informatie
 */
if ((isset($_GET["d"]) && $_GET["d"] === "users") || !isset($_GET['d'])) {
    echo "<table>";
    echo "<tr>";
    echo "<td width='50px'>ID: </td>";
    echo "<td width='300px'>Leerling/Docent nummer </td>";
    echo "<td width='300px'>Voornaam: </td>";
    echo "<td width='300px'>Achternaam: </td>";
    echo "<td width='300px'>Email: </td>";
    echo "<td width='300px'>Datum geregistreerd </td>";
    echo "</tr>";
    $users = $userController->getAllUsers();
    if (!empty($users)) {
        foreach ($users as $user) {
            if ($user instanceof \ThemaC\User) {
                echo "<tr>";
                echo "<td>" . $user->getId() . "</td>";
                echo "<td>" . $user->getCode() . "</td>";
                echo "<td>" . $user->getFirstName() . "</td>";
                echo "<td>" . $user->getLastName() . "</td>";
                echo "<td>" . $user->getEmail() . "</td>";
                echo "<td>" . $user->getRegisterDate() . "</td>";
                echo "</tr>";
            }
        }
    }
    echo "</table>";

} /**
 * GET "videos"
 * Van dashboard naar videos informatie
 */

else if (isset($_GET["d"]) && $_GET["d"] === "videos") {
    if(isset($_GET['action']) && $_GET['action'] === "delete") {
        if(isset($_GET['v']))
            $videoController->removeVideo($videoController->getVideoById($_GET['v']));
    }
    echo "<table>";
    echo "<tr>";
    echo "<td width='50px'> ID: </td>";
    echo "<td width='300px'>Naam: </td>";
    echo "<td width='200px'>Categorie: </td>";
    echo "<td width='300px'>Beschrijving: </td>";
    echo "<td width='200px'>Uploader: </td>";
    echo "<td width='100px'>Views: </td>";
    echo "<td width='100px'></td>";
    echo "</tr>";
    $videos = $videoController->getAllVideos();
    if (!empty($videos)) {
        foreach ($videos as $video) {
            if ($video instanceof \ThemaC\Video) {
                $category = $categoryController->getCategoryById($video->getCategory());
                $uploader = $userController->getUserById($video->getUploader());
                echo "<tr>";
                echo "<td>" . $video->getId() . "</td>";
                echo "<td><div class='textbox'>" . $video->getName() . "</div></td>";
                echo "<td><div>" . $category->getName() . "</div></td>";
                echo "<td><div class='textbox'>" . $video->getDescription() . "</div></td>";
                echo "<td><div>" . $uploader->getFirstName() . " " . $uploader->getLastName() . "</div></td>";
                echo "<td><div class='textbox'>" . $video->getViews() . "</div></td>";
                echo "<td><a href='" . \ThemaC\MainController::SafePageName() . "?d=videos&action=delete&v=" . $video->getId() . "' onclick='return confirm(\"Weet je zeker dat je de video " . $video->getName() . " wilt verwijderen?\");'>Verwijderen</a></td>";
                echo "</tr>";
            }
        }
    }
    echo "</table>";

} /**
 * GET "adduser"
 * Van dashboard naar user toevoegen
 */
else if (isset($_GET["d"]) && $_GET["d"] === "adduser") { ?>
    <form action="<?php echo \ThemaC\MainController::SafePageName(); ?>" method="POST">
        <table>

            <tr>
                <td width="200px"><label for="code">
                        Studentnummer
                    </label></td>
                <td><input type="text" name="code" id="code" placeholder="69696"/></td>
            </tr>
            <tr>
                <td><label for="firstName">
                        Voornaam
                    </label></td>
                <td><input type="text" name="firstName" id="firstName" placeholder="Kees"/></td>
            </tr>
            <tr>
                <td><label for="lastName">
                        Achternaam
                    </label>
                <td><input type="text" name="lastName" id="lastName" placeholder="de Leeuw"/></td>
            </tr>
            <tr>
                <td><label for="email">
                        Email
                    </label>
                <td><input type="email" name="email" id="email" placeholder="69696@glr.nl"/></td>
            </tr>
            <tr>
                <td><label for="password">
                        Wachtwoord
                    </label>
                <td><input type="password" name="password" id="password"
                           placeholder="CorrectHorseBatteryStaple"/></td>
            </tr>
            <tr>
                <td><input type="hidden" name="adduser" value="adduser"></td>
                <td><input type="submit"> <input type="reset"></td>
            </tr>
        </table>
    </form>

    <?php

    /**
     * GET "adduser"
     * Van dashboard naar user toevoegen
     */
} else if (isset($_GET["d"]) && $_GET["d"] === "adduser") { ?>
    <form action="<?php echo \ThemaC\MainController::SafePageName(); ?>" method="POST">
        <table>

            <tr>
                <td width="200px"><label for="code">
                        Studentnummer
                    </label></td>
                <td><input type="text" name="code" id="code" placeholder="69696"/></td>
            </tr>
            <tr>
                <td><label for="firstName">
                        Voornaam
                    </label></td>
                <td><input type="text" name="firstName" id="firstName" placeholder="Kees"/></td>
            </tr>
            <tr>
                <td><label for="lastName">
                        Achternaam
                    </label>
                <td><input type="text" name="lastName" id="lastName" placeholder="de Leeuw"/></td>
            </tr>
            <tr>
                <td><label for="email">
                        Email
                    </label>
                <td><input type="email" name="email" id="email" placeholder="69696@glr.nl"/></td>
            </tr>
            <tr>
                <td><label for="password">
                        Wachtwoord
                    </label>
                <td><input type="password" name="password" id="password"
                           placeholder="CorrectHorseBatteryStaple"/></td>
            </tr>
            <tr>
                <td><input type="hidden" name="adduser" value="adduser"></td>
                <td><input type="submit"> <input type="reset"></td>
            </tr>
        </table>
    </form>
    <?php

} /**
 * GET "changeuser"
 * Van dashboard naar change user
 */

else if (isset($_GET["d"]) && $_GET["d"] === "changeuser") {
    if (isset($_GET['action'])) {
        if ($_GET['action'] === "delete") {
            $userController->removeUser($_GET['u']);
            // TODO: Geef een bericht weer
        }
        if ($_GET['action'] === "edit") {
            // TODO: process user edit
        }
    }
    echo "<table>";
    echo "<tr>";
    echo "<td width='50px'>ID: </td>";
    echo "<td width='200px'>Leerling/Docent </td>";
    echo "<td width='300px'>Naam: </td>";
    echo "<td width='150px'></td>";
    echo "<td width='150px'></td>";
    echo "</tr>";
    $users = $userController->getAllUsers();
    if (!empty($users)) {
        foreach ($users as $user) {
            if ($user instanceof \ThemaC\User) {
                echo "<tr>";
                echo "<td>" . $user->getId() . "</td>";
                echo "<td>" . $user->getCode() . "</td>";
                echo "<td>" . $user->getFirstName() . " " . $user->getLastName() . "</td>";
                echo "<td><a href='" . \ThemaC\MainController::SafePageName() . "?d=changeuser&action=edit&u=" . $user->getId() . "'>Wijzigen</a></td>";
                echo "<td><a href='" . \ThemaC\MainController::SafePageName() . "?d=changeuser&action=delete&u=" . $user->getId() . "' onclick='return confirm(\"Weet je zeker dat je de gebruiker " . $user->getFirstName() . " wilt verwijderen?\");'>Verwijderen</a></td>";
                echo "</tr>";
            }
        }
    }
    echo "</table>";
} /**
 * GET "categories"
 * Van dashboard naar categorieën
 */

else if (isset($_GET["d"]) && $_GET["d"] === "categories") {
    echo "<table>";
    echo "<tr>";
    echo "<td width='50px'> ID: </td>";
    echo "<td width='300px'>Naam: </td>";
    echo "<td width='300px'>Beschrijving: </td>";
    echo "</tr>";
    $categories = $categoryController->getAllCategories();
    if (!empty($categories)) {
        foreach ($categories as $category) {
            if ($category instanceof \ThemaC\Category) {
                echo "<tr>";
                echo "<td>" . $category->getId() . "</td>";
                echo "<td><div class='textbox'>" . $category->getName() . "</div></td>";
                echo "<td><div class='textbox'>" . $category->getDescription() . "</div></td>";
                echo "</tr>";
            }
        }
    }
    echo "</table>";
} /**
 * GET "upload"
 * Van dashboard naar uploaden
 */
else if (isset($_GET["d"]) && $_GET["d"] === "upload") { ?>
    <form action="<?php echo \ThemaC\MainController::SafePageName(); ?>" method="POST"
          enctype="multipart/form-data">
        <label for="file">Video bestand:</label> <input type="file" name="video" id="file"/><br/>
        <label for="name">Video titel:</label> <input type="text" name="name" id="name"/><br/>
        <label for="description">Beschrijving:</label> <textarea name="description" id="description"></textarea><br/>
        <label for="categoryField">Category:</label><select name="category" id="categoryField">
            <option disabled selected value="0" title="haha!">Kies een category</option>
            <?php
            $categories = $categoryController->getAllCategories();
            foreach ($categories as $category) { ?>
                <option value="<?php echo $category->getId(); ?>"
                        title="<?php echo $category->getDescription(); ?>"><?php echo $category->getName(); ?></option>
            <?php } ?>
        </select><br/>
        <input type="hidden" name="uploadvideo" value="uploadvideo">
        <input type="submit">
    </form>
    <?php
} else {
    ?>
    Selecteer een optie aan de rechter kant
    <?php
}
?>