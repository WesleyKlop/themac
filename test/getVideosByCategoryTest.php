<?php
/**
 * Created by PhpStorm.
 * User: Wesley
 * Date: 28-1-2016
 * Time: 09:46
 */
require_once "../inc/includes.php";
?>
<form action="<?php echo \ThemaC\MainController::SafePageName(); ?>" method="POST" class="test">
    <input type="text" placeholder="Category naam" name="catName"/>
    <input type="submit"/>
</form>
<pre class="test"><?php
    if ($_SERVER['REQUEST_METHOD'] === "POST" && isset($_POST['catName']) && !empty($_POST['catName'])) {
        $cat = $_POST['catName'];
        echo "Category: " . $cat . PHP_EOL;

        // Haal categorieen op via ID
        var_dump($videoController->getVideosByCategory($cat));
        echo "---------------------------------\n";
        // Haal categorieen op via Category Class
        $cat = $categoryController->getCategoryById($cat);
        var_dump($videoController->getVideosByCategory($cat));
        echo "---------------------------------\n";
        var_dump($cat);
    }
    ?></pre>