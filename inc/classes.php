<?php
/**
 * Created by PhpStorm.
 * User: Wesley
 * Date: 7-1-2016
 * Time: 11:30
 */

namespace ThemaC;

use getID3;
use PDO;
use PDOException;

/**
 * Class MainController
 * @package ThemaC
 */
class MainController
{
    /**
     * De DOCUMENT_ROOT waar de web files zich bevinden
     */
    const DOCUMENT_ROOT = '/home/video2/public_html/';

    /**
     * Hier worden alle queries op uitgevoerd
     * @var PDO $dbh database handler waar alle queries op uitgevoerd worden
     */
    protected $dbh;

    /**
     * MainController constructor.
     * Hierin word de database interface gekoppelt aan een interne variabele
     * @param PDO $db database die gebruikt word om alle queries op uit te voeren
     */
    public function __construct(&$db)
    {
        $this->dbh = $db;
    }

    /**
     * Geeft de url van de pagina voor gebruik in forms
     * @return string pagina naam voor html forms
     */
    public static function SafePageName()
    {
        return htmlspecialchars($_SERVER['PHP_SELF']);
    }

    /**
     * Voegt een view toe aan een video
     * @param int $videoId de ID van de video
     * @return int 1 on success or -1 if empty videoid or -2 if database error
     */
    public function addView($videoId)
    {
        // Check of de videoId meegegeven is
        if (empty($videoId)) {
            return -1;
        }

        // UPDATE de views van de video met de bijbehorende id
        $stmt = $this->dbh->prepare("UPDATE videos SET views = views + 1 WHERE id = :id");
        $stmt->bindParam(":id", $videoId, PDO::PARAM_INT);

        // Execute de query
        try {
            $stmt->execute();
        } catch (PDOException $e) {
            return -2;
        }

        // Het is gelukt :D
        return 1;
    }

    /**
     * Ordered de files array zodat het $array['veldnaam']['property'] word ipv de standaard
     * @param array $input de array om te sorteren
     * @param bool $top als het bestand aan de top moet(word intern gebruikt)
     * @return array de georderde array
     */
    protected function orderFilesArray($input, $top = true)
    {
        $files = array();
        foreach ($input as $name => $file) {
            if ($top) $sub_name = $file['name'];
            else $sub_name = $name;

            if (is_array($sub_name)) {
                foreach (array_keys($sub_name) as $key) {
                    $files[$name][$key] = array(
                        'name' => $file['name'][$key],
                        'type' => $file['type'][$key],
                        'tmp_name' => $file['tmp_name'][$key],
                        'error' => $file['error'][$key],
                        'size' => $file['size'][$key],
                    );
                    $files[$name] = self::orderFilesArray($files[$name], false);
                }
            } else {
                $files[$name] = $file;
            }
        }
        return $files;
    }

    /**
     * Maakt een thumbnail bestand van de video op het pad $video
     * @param string $video het pad naar de tmp video bestand
     * @param int $videoId de id van de video
     * @param float $videoLength de lengte van de video in seconde
     * @return true
     */
    protected function makeThumbNail($video, $videoId, $videoLength)
    {
        $thumbnailTime = gmdate($videoLength / 2);
        $outputFile = escapeshellarg(Video::THUMBNAIL_DIRECTORY . $videoId . ".png");
        $command = "avconv -i " . escapeshellarg($video) . " -ss " . $thumbnailTime . " -vframes 1 " . $outputFile;
        shell_exec($command);
        return true;
    }
}

/**
 * Class CategoryController
 * @package ThemaC
 */
class CategoryController extends MainController
{
    /**
     * UserController constructor.
     * @var PDO $dbh database handler waar alle queries op uitgevoerd worden
     */
    public function __construct(&$dbh)
    {
        parent::__construct($dbh);
    }

    /**
     * Voegt een categorie toe aan de database en returned deze
     * @param string $name naam van category
     * @param string $description beschrijving van category
     * @return bool|Category in eenstance van de Category Class met bijbehorende info, of false
     */
    public function addCategory($name, $description)
    {
        if (empty($name)
            || empty($description)
        ) {
            return false;
        }

        if ($this->getCategoryByName($name) instanceof Category) {
            return false;
        }

        $stmt = $this->dbh->prepare("INSERT INTO categories(name, description) VALUES (:catName, :catDesc)");
        $stmt->bindParam(":catName", $name);
        $stmt->bindParam(":catDesc", $description);

        try {
            $stmt->execute();
        } catch (PDOException $e) {
            return false;
        }

        return $this->getCategoryByName($name);
    }

    /**
     * Haalt categorie op bij naam
     * @param string $name naam van de categorie om op te halen
     * @return bool|Category de categorie in een Category class of false als de categorie niet bestaat
     */
    public function getCategoryByName($name)
    {
        if (empty($name)) {
            return false;
        }

        $stmt = $this->dbh->prepare("SELECT * FROM categories WHERE name = :catName");
        $stmt->bindParam(":catName", $name);

        try {
            $stmt->execute();
        } catch (PDOException $e) {
            return false;
        }

        $data = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($data['name'] != $name)
            return false;

        // Return een User class met alle data
        return new Category($data['id'], $data['name'], $data['description']);
    }

    /**
     * Haalt categorie op bij id
     * @param int $id de id van de categorie
     * @return bool|Category de categorie in een Category class of false als de categorie niet bestaat
     */
    public function getCategoryById($id)
    {
        if (empty($id)) {
            return false;
        }

        $stmt = $this->dbh->prepare("SELECT * FROM categories WHERE id = :id");
        $stmt->bindParam(":id", $id);

        try {
            $stmt->execute();
        } catch (PDOException $e) {
            return false;
        }

        $data = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($data['id'] != $id)
            return false;

        // Return een User class met alle data
        return new Category($data['id'], $data['name'], $data['description']);
    }

    /**
     * Haalt alle categorieën op
     * @return Category[]|bool de array met categorieën of false als het niet lukt
     */
    public function getAllCategories()
    {
        $stmt = $this->dbh->prepare("SELECT * FROM categories");

        try {
            $stmt->execute();
        } catch (PDOException $e) {
            return false;
        }

        $categories = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $results = array();

        foreach ($categories as $category) {
            array_push($results, new Category($category['id'], $category['name'], $category['description']));
        }

        return $results;
    }
}


/**
 * Class VideoController
 * @package ThemaC
 */
class VideoController extends MainController
{
    /**
     * UserController constructor.
     * @var PDO $dbh database handler waar alle queries op uitgevoerd worden.
     */
    public function __construct(&$dbh)
    {
        parent::__construct($dbh);
    }

    /**
     * Verwijderd een video van de database en van het systeem
     * @param Video $video de video die verwijderd moet worden
     * @return int 1 bij success of anders errorcode
     */
    public function removeVideo($video) {
        if(!($video instanceof Video))
            return -1;

        // Kijk of video bestand en thumbnail file bestaan
        if(!file_exists($video->getVideoSystemPath())
        || !file_exists($video->getThumbNailSystemPath()))
            return -3;

        $stmt = $this->dbh->prepare("DELETE FROM videos WHERE id = :vid");
        $videoId = $video->getId();
        $stmt->bindParam(":vid", $videoId);

        try {
            $stmt->execute();
        } catch(PDOException $e) {
            return -2;
        }

        // Verwijderen van db gelukt, verwijder nu de bestanden
        unlink($video->getThumbNailSystemPath());
        unlink($video->getVideoSystemPath());

        return 1;
    }

    /**
     * Deze functie upload de video naar de server, plaatst de data in de videos tabel en encode de video met avconv
     * @param User|bool $user de gebruiker die de video upload
     * @param string $videoName de naam van de video
     * @param string $videoDescription de beschrijving van de video
     * @param Category $videoCategory de categorie van de video
     * @param array $video de $_FILES array welke de video bevat
     * @param string $videoKey de name van de video input
     * @return int|Video
     */
    public function uploadVideo($user, $videoName, $videoDescription, $videoCategory, $video, $videoKey = 'video')
    {
        // Check of alles meegegeven is
        if (!($user instanceof User)
            || !($videoCategory instanceof Category)
            || empty($videoName)
            || empty($videoDescription)
            || empty($video)
            || empty($videoKey)
        ) {
            return -1;
        }

        $videoName = trim($videoName);
        $videoDescription = trim($videoDescription);

        // Maak de video array wat overzichtelijker en haal meteen de enige relevante array eruit
        $video = $this->orderFilesArray($video)[$videoKey];

        // Toegestaane filetypes
        $regex = '/^video\/[a-z0-9\-]+$/';
        if (!preg_match($regex, $video["type"])) {
            return -2;
        }

        // Haal de video lengte op
        $getID3 = new getID3();
        $file = $getID3->analyze($video['tmp_name']);
        // Als ID3 de tags niet op kan halen return dan false
        if ($file == false) return -3;
        $videoLength = $file["playtime_seconds"];

        // Plaats de video in de database
        $stmt = $this->dbh->prepare("INSERT INTO videos(uploaderId, categoryId, name, description, length, uploadTime) VALUES (:uid, :cid, :vidName, :vidDesc, SEC_TO_TIME(:vidLength), FROM_UNIXTIME(:vidUploadTime))");
        $userId = $user->getId();
        $stmt->bindParam(":uid", $userId);
        $categoryId = $videoCategory->getId();
        $stmt->bindParam(":cid", $categoryId);
        $stmt->bindParam(":vidName", $videoName);
        $stmt->bindParam(":vidDesc", $videoDescription);
        $stmt->bindParam(":vidLength", $videoLength);
        // $_SERVER['REQUEST_TIME'] is een unix timestamp van de tijd van de request, dit word in de query naar timestamp geconverteert
        $stmt->bindParam(":vidUploadTime", $_SERVER['REQUEST_TIME']);

        try {
            $stmt->execute();
        } catch (PDOException $e) {
            return -4;
        }

        // Haal de net geplaatste video op uit de database
        $videoId = $this->dbh->lastInsertId();

        // Maak de thumbnail
        $this->makeThumbNail($video['tmp_name'], $videoId, $videoLength);

        // Convert nu het bestand... dit kan even duren
        $outputFile = Video::VIDEO_DIRECTORY . $videoId . ".mp4";
        $command = "avconv -i " . escapeshellarg($video['tmp_name']) . " -strict experimental " . escapeshellarg($outputFile);
        shell_exec($command);

        // Return als alles klaar is het video object
        return $this->getVideoById($videoId);
    }

    /**
     * Haalt video op met de ID
     * @param int $vid de id van de video
     * @return bool|Video false als de video niet gevonden is of de Video met alle data
     */
    public function getVideoById($vid)
    {
        // Check of er wel een variabele mee is gegeven
        if (empty($vid))
            return false;

        // Prepare de database Query
        $stmt = $this->dbh->prepare("SELECT * FROM videos WHERE id = :id");
        $stmt->bindParam(":id", $vid);

        try {
            $stmt->execute();
        } catch (PDOException $e) {
            return false;
        }

        // Er kan maar 1 result zijn want id is PRIMARY_KEY, dus gebruik fetch ipv fetchAll
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        // Check of er een result is
        if ($result['id'] != $vid)
            return false;

        // Return een Video class object met de resultaten
        return self::createVideoFromVariables($result['id'], $result['name'], $result['description'], $result['length'],
            $result['uploaderId'], $result['uploadTime'], $result['views'], $result['categoryId']);
    }

    /**
     * Maakt een video object met de gegeven variabelen
     * @param int $id
     * @param string $name
     * @param string $description
     * @param string $length
     * @param string $uploader
     * @param string $uploadTime
     * @param int $views
     * @param int $category
     * @return Video
     */
    private static function createVideoFromVariables($id, $name, $description, $length, $uploader, $uploadTime, $views, $category)
    {
        return new Video($id, $name, $description, $length, $uploader, $uploadTime, $views, $category);
    }

    /**
     * Haalt aantal recente video's op uit tabel
     * @param int $amount aantal recente videos om op te halen
     * @return Video[]|bool de array met videos of false bij een error
     */
    public function getRecentVideos($amount = 3)
    {
        $stmt = $this->dbh->prepare("SELECT * FROM videos ORDER BY uploadTime DESC LIMIT :amount");
        $stmt->bindParam(":amount", $amount, PDO::PARAM_INT);

        try {
            $stmt->execute();
        } catch (PDOException $e) {
            return false;
        }

        $videos = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $results = array();

        foreach ($videos as $video) {
            $results[] = self::createVideoFromVariables($video['id'], $video['name'], $video['description'], $video['length'],
                $video['uploaderId'], $video['uploadTime'], $video['views'], $video['categoryId']);
        }

        return $results;
    }

    /**
     * Haalt alle videos op uit tabel
     * @return Video[]|bool de array met alle videos's of false als het niet lukt
     */
    public function getAllVideos()
    {
        $stmt = $this->dbh->prepare("SELECT * FROM videos");

        try {
            $stmt->execute();
        } catch (PDOException $e) {
            return false;
        }

        $videos = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $results = array();

        foreach ($videos as $video) {
            $results[] = self::createVideoFromVariables($video['id'], $video['name'], $video['description'], $video['length'],
                $video['uploaderId'], $video['uploadTime'], $video['views'], $video['categoryId']);
        }

        return $results;
    }

    /**
     * Haalt alle videos gelimiteerd door $limit op op basis van de category
     * @param Category|int $cat category class of de id van de category
     * @param int $limit aantal video's om op te halen
     * @return int|Video[] Errorcode of de array met videos
     */
    public function getVideosByCategory($cat, $limit = 3)
    {
        if (empty($cat)
            || empty($limit)
        ) {
            return -1;
        }

        // Als category een category class is willen we de ID eruit halen
        $cat = ($cat instanceof Category) ? $cat->getId() : $cat;

        if (is_string($limit)) {
            $limit = 1337;
        }

        $stmt = $this->dbh->prepare("SELECT * FROM videos WHERE categoryId = :cat ORDER BY uploadTime DESC LIMIT :lim");
        $stmt->bindParam(":cat", $cat, PDO::PARAM_INT);
        $stmt->bindParam(":lim", $limit, PDO::PARAM_INT);

        try {
            $stmt->execute();
        } catch (PDOException $e) {
            return -2;
        }

        $results = [];
        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $video) {
            $results[] = self::createVideoFromVariables($video['id'], $video['name'], $video['description'], $video['length'],
                $video['uploaderId'], $video['uploadTime'], $video['views'], $video['categoryId']);
        }


        return $results;
    }

    /**
     * Haalt alle video's van een gebruiker op
     * @param User|int $user de gebruiker waarvan video's opgehaald moeten worden
     * @return Video[]|int errorcode of de array met results
     */
    public function getVideosByUser($user)
    {

        if (empty($user))
            return -1;

        // Als category een category class is willen we de ID eruit halen
        $user = ($user instanceof User) ? $user->getId() : $user;

        $stmt = $this->dbh->prepare("SELECT * FROM videos WHERE uploaderId = :uid ORDER BY uploadTime DESC");
        $stmt->bindParam(":uid", $user, PDO::PARAM_INT);

        try {
            $stmt->execute();
        } catch (PDOException $e) {
            return -2;
        }

        $results = [];
        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $video) {
            $results[] = self::createVideoFromVariables($video['id'], $video['name'], $video['description'], $video['length'],
                $video['uploaderId'], $video['uploadTime'], $video['views'], $video['categoryId']);
        }

        return $results;
    }

    /**
     * @param string $name name to search
     * @return int|Video[] results or error
     */
    public function getVideosByName($name)
    {
        if (empty($name))
            return -1;

        $stmt = $this->dbh->prepare("SELECT * FROM videos WHERE name LIKE :vidname");
        $vidname = '%' . $name . '%';
        $stmt->bindParam(":vidname", $vidname);

        try {
            $stmt->execute();
        } catch (PDOException $e) {
            return -2;
        }

        $results = [];
        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $video) {
            $results[] = self::createVideoFromVariables($video['id'], $video['name'], $video['description'], $video['length'],
                $video['uploaderId'], $video['uploadTime'], $video['views'], $video['categoryId']);
        }

        return $results;
    }
}

/**
 * Class UserController
 * @package ThemaC
 */
class UserController extends MainController
{
    /**
     * UserController constructor.
     * @var PDO $dbh database handler waar alle queries op uitgevoerd worden.
     */
    public function __construct(&$dbh)
    {
        parent::__construct($dbh);
    }

    /**
     * Haalt alle users op
     * @return User[]|bool de array met users of false als het niet lukt
     */
    public function getAllUsers()
    {
        $stmt = $this->dbh->prepare("SELECT * FROM users ORDER BY code DESC");

        try {
            $stmt->execute();
        } catch (PDOException $e) {
            return false;
        }

        $users = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $results = [];

        foreach ($users as $user) {
            $results[] = new User($user['id'], $user['firstname'], $user['lastname'], $user['email'],
                $user['password'], $user['date'], $user['code']);
        }

        return $results;
    }

    /**
     * Verwijderd een user
     * @param int|User $user de id van de user of de User class
     * @return int success code(1) of een error code
     */
    public function removeUser($user)
    {
        if (empty($user))
            return -1;

        // Check of de user bestaat(als het een id is)
        if (!($user instanceof User)) {
            $user = $this->getUserById($user);
            // User bestaat niet, return -3
            if (!($user instanceof User))
                return -3;
        }
        // Haal de user ID op
        $uid = $user->getId();

        // TODO: Verwijder video's van gebruiker

        // Prepare de query
        $stmt = $this->dbh->prepare("DELETE FROM users WHERE id = :uid");
        $stmt->bindParam(":uid", $uid);

        try {
            $stmt->execute();
        } catch (PDOException $e) {
            return -2;
        }

        return 1;
    }

    /**
     * Haalt een User uit de database met de gekozen ID
     * @param int $id userid van de gebruiker
     * @return bool|User de User of false
     */
    public function getUserById($id)
    {
        // Check of het id mee gegeven is
        if (empty($id)) {
            return false;
        }

        // Haal alle row data uit de table
        $stmt = $this->dbh->prepare("SELECT * FROM users WHERE id = :id");
        $stmt->bindParam(":id", $id);

        try {
            $stmt->execute();
        } catch (PDOException $e) {
            return false;
        }

        // Er kan maar 1 result uit komen dus is het niet nodig om fetchAll te gebruiken
        $data = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($data['id'] != $id)
            return false;

        // Return een User class met alle data
        return new User($data['id'], $data['firstname'], $data['lastname'], $data['email'], $data['password'], $data['date'], $data['code']);
    }

    /**
     * Logt de gebruiker in en geeft de gegevens van de gebruiker terug
     * @param string $email email invoer van gebruiker
     * @param string $password wachtwoord invoer van gebruiker
     * @return bool true bij succesvolle inlog anders false
     */
    public function loginUser($email, $password)
    {
        if (empty($email)
            || empty($password)
        ) {
            return false;
        }
        // Check of de gebruiker bestaat
        $user = $this->getUserByEmail($email);
        if ($user instanceof User) {
            // gebruiker bestaat, controleer wachtwoord
            if (password_verify($password, $user->getPassHash())) {
                // return the user
                return $user;
            }
        }
        return false;
    }

    /**
     * Haalt een user uit de database via de $email en geeft deze terug in een User class
     * @param string $email de email om te zoeken
     * @return bool|User de User of false
     */
    public function getUserByEmail($email)
    {
        if (empty($email)) {
            return -1;
        }

        $stmt = $this->dbh->prepare("SELECT * FROM users WHERE email = :email");
        $stmt->bindParam(":email", $email);

        try {
            $stmt->execute();
        } catch (PDOException $e) {
            return -2;
        }

        $data = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($data['email'] != $email)
            return false;

        // Return een User class met alle data
        return new User($data['id'], $data['firstname'], $data['lastname'], $data['email'], $data['password'], $data['date'], $data['code']);
    }

    /**
     * @param string $name
     * @return User[]|int de array met gevonden users of errorcode
     */
    public function getUsersByName($name)
    {
        if (empty($name)) {
            return -1;
        }

        $fName = '%' . $name . '%';

        $stmt = $this->dbh->prepare("SELECT * FROM users WHERE firstname LIKE :fName OR lastname LIKE :fName");
        $stmt->bindParam(":fName", $fName);

        try {
            $stmt->execute();
        } catch (PDOException $e) {
            return -2;
        }

        $users = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $result = [];

        foreach ($users as $user) {
            $result[] = new User($user['id'], $user['firstname'], $user['lastname'], $user['email'], $user['password'], $user['date'], $user['code']);
        }

        return $result;
    }

    /**
     * Deze methode creeërt gebruikers en returned de User, de \PDOexception errorcode of 0
     * @param string $firstName voornaam van de gebruiker
     * @param string $lastName achternaam van de gebruiker
     * @param string $email Email van de gebruiker
     * @param string $password Nog niet gehashde wachtwoord van de gebruiker
     * @param string|int $code Studentcode of leeraar nummer
     * @return bool|User de gebruiker of false
     */
    public function addUser($firstName, $lastName, $email, $password, $code)
    {
        // Valideer input om te kijken of niks leeg is
        if (empty($firstName)
            || empty($lastName)
            || empty($email)
            || empty($password)
            || empty($code)
        ) {
            return false;
        }

        // Check of user al bestaat
        if ($this->getUserByEmail($email) instanceof User) {
            return false;
        }

        // Input correct, voeg user toe aan database
        $stmt = $this->dbh->prepare(
            "INSERT INTO users(email, firstname, lastname, password, code) "
            . "VALUES (:email, :first, :last, :password, :code)"
        );

        // bind de email, voornaam, achternaam en studentnummer
        $stmt->bindParam(":email", $email);
        $stmt->bindParam(":first", $firstName);
        $stmt->bindParam(":last", $lastName);
        $stmt->bindParam(":code", $code);
        // hash het wachtwoord
        $password = password_hash($password, PASSWORD_DEFAULT);
        // and in the darkness bind them
        $stmt->bindParam(":password", $password);

        try {
            // execute de query
            $stmt->execute();
        } catch (PDOException $e) {
            return false;
        }

        // Insert gelukt, haal de user uit de database voor de auto gegenereerde velden
        return $this->getUserById($this->dbh->lastInsertId());
    }
}

/**
 * Class Video
 * @package ThemaC
 */
class Video
{
    /**
     * Dit is de directory die alle video's bevat
     */
    const VIDEO_DIRECTORY = MainController::DOCUMENT_ROOT . "videos/";
    /**
     * De directory met video thumbnails
     */
    const THUMBNAIL_DIRECTORY = MainController::DOCUMENT_ROOT . "assets/thumbnails/";
    /**
     * @var int $id de id van de video
     */
    private $id;
    /**
     * @var string $name de naam van de video
     */
    private $name;
    /**
     * @var string $description de beschrijving van de video
     */
    private $description;
    /**
     * @var string $length de lengte van de video
     */
    private $length;
    /**
     * @var int $uploader de uploader van de video
     */
    private $uploader;
    /**
     * @var string de opload tijd van de video
     */
    private $uploadTime;
    /**
     * @var int $views aantal keren dat de video bekeken is
     */
    private $views;
    /**
     * @var int $category de categorie aan welke de video toebehoord
     */
    private $category;

    /**
     * Video constructor.
     * @param int $id de id van de video
     * @param string $name de naam van de video
     * @param string $description de beschrijving van de video
     * @param string $length de lengte van de video
     * @param int $uploader de uploader van de video
     * @param string $uploadTime de upload tijd van de video
     * @param int $views aantal keren dat de video bekeken is
     * @param int $category de categorie aan welke de video toebehoord
     */
    public function __construct($id, $name, $description, $length, $uploader, $uploadTime, $views, $category)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->length = $length;
        $this->uploader = $uploader;
        $this->uploadTime = $uploadTime;
        $this->views = $views;
        $this->category = $category;
    }

    /**
     * @return bool|string het pad naar de video op het systeem of false als het niet past
     */
    public function getVideoSystemPath()
    {
        $path = self::VIDEO_DIRECTORY . $this->getVideoFileName();
        if (file_exists($path)) {
            return $path;
        }
        return false;
    }

    /**
     * @return string geeft de naam van het video bestand(id + .mp4)
     */
    private function getVideoFileName()
    {
        return $this->id . ".mp4";
    }

    /**
     * @return string het pad naar de locatie van de video op het web
     */
    public function getVideoWebPath()
    {
        return "/video.php?v=" . $this->id;
    }

    /**
     * @return string de beschrijving van de video
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string lengte van de video
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @return int views van de video
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * @return int category van de video
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param UserController $userController
     * @param bool $withLi if the html should be wrapped in an li element
     * @return string html formatted video
     */
    public function getHtml($userController, $withLi = false)
    {
        $user = $userController->getUserById($this->getUploader());
        $userFirstName = substr($user->getFirstName(), 0, 1);
        $userLastName = $user->getLastName();
        $result = "";

        $result .= ($withLi) ? "<li>" : "";
        $result .= "<div class='videopreview'>";
        $result .= "<a href='/videopage.php?v={$this->getId()}'>";
        $result .= "<img src='{$this->getVideoThumbNail()}' alt='Preview1'>";
        $result .= "</a>";
        $result .= "<div class='videobeschrijving'>";
        $result .= "<div class='videopreview_title'><a href='/videopage.php?v={$this->getId()}'>{$this->getName()}</a></div>";
        $result .= "<div class='videopreview_owner'><a href='/profile.php?id={$user->getId()}'>{$userFirstName}. {$userLastName}</a></div>";
        $result .= "<div class='videopreview_date'><span>{$this->getUploadTime()}</span></div>";
        $result .= "</div>";
        $result .= "</div>";
        $result .= ($withLi) ? "</li>" : "";

        return $result;
    }

    /**
     * @return int
     */
    public function getUploader()
    {
        return $this->uploader;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return bool|string de string met het pad naar de thumbnail of false als de thumbnail niet bestaat
     */
    public function getVideoThumbNail()
    {
        $file = $this->id . ".png";
        if (file_exists(self::THUMBNAIL_DIRECTORY . $file)) {
            return "/assets/thumbnails/" . $file;
        }
        return false;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return \DateTime
     */
    public function getUploadTime()
    {
        return $this->uploadTime;
    }

    public function getThumbNailSystemPath()
    {
        return self::THUMBNAIL_DIRECTORY . $this->id . ".png";
    }
}

/**
 * Class Category
 * Bevat informatie over een categorie
 * @package ThemaC
 */
class Category
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $description;

    /**
     * Category constructor.
     * @param int $id
     * @param string $name
     * @param string $description
     */
    public function __construct($id, $name, $description)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
    }

    /**
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }
}

/**
 * Class User
 * @package ThemaC
 */
class User
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $firstName;
    /**
     * @var string
     */
    private $lastName;
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $passHash;
    /**
     * @var string
     */
    private $registerDate;
    /**
     * @var string $code de code(nummer of naam) van de gebruiker(bijvoorbeeld 77562 of KTH)
     */
    private $code;
    /**
     * @var bool $hasUploadRights om te kijken of de gebruiker video's mag uploaden
     */
    private $hasUploadRights;

    /**
     * User constructor.
     * @param int $id
     * @param string $firstName
     * @param string $lastName
     * @param string $email
     * @param string $passHash
     * @param string $registerDate
     * @param string|int $code
     */
    public function __construct($id, $firstName, $lastName, $email, $passHash, $registerDate, $code)
    {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->registerDate = $registerDate;
        $this->passHash = $passHash;
        $this->code = $code;
        $this->hasUploadRights = (bool)preg_match("/^([A-Za-z]+|[^0-9]+)$/", $this->code);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassHash()
    {
        return $this->passHash;
    }

    /**
     * @return string
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return boolean
     */
    public function getUploadRights()
    {
        return $this->hasUploadRights;
    }
}