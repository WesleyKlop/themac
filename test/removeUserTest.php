<?php
/**
 * Created by PhpStorm.
 * User: Wesley
 * Date: 12-1-2016
 * Time: 14:31
 */
require_once "../inc/includes.php";
if ($_SERVER['REQUEST_METHOD'] === "POST") {
    $res = $userController->removeUser($_POST['uid']);
    echo "<pre>";
    var_dump($res);
    echo "</pre>";
}
?>

<form action="<?php echo \ThemaC\MainController::SafePageName(); ?>" method="POST">
    <label>User ID:
        <input type="number" name="uid"/>
    </label>
    <input type="submit"/>
</form>
