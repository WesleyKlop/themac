<?php
/**
 * Created by PhpStorm.
 * User: 77711
 * Date: 7-1-2016
 * Time: 11:09
 */
require_once "inc/database.php";
require_once "inc/getid3/getid3.php";
require_once "inc/classes.php";
require_once "inc/session.php";

$userController = new \ThemaC\UserController($dbh);

if ($_SERVER['REQUEST_METHOD'] === "POST") {
    $success = $userController->loginUser($_POST['email'], $_POST['password']);

    if ($success instanceof \ThemaC\User) {
        $_SESSION['user'] = $success;
        header("Location: /home");
        exit;
    }
}

if (isset($_SESSION['user']) && $_SESSION['user'] instanceof \ThemaC\User) {
    header("Location: " . ((!empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : "/home"));
    exit;
}

?><!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="icon" sizes="192x192" href=""/>
    <title>Mediatech video&apos;s | <?php echo ucfirst(basename($_SERVER['PHP_SELF'], ".php")); ?></title>
    <link href="https://fonts.googleapis.com/css?family=Material+Icons%7CRoboto" rel="stylesheet"/>
    <link rel="stylesheet" href="/assets/main.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="background">
<div class="login">
    <div class="indexImage">
    </div>
    <?php if (isset($success) && $success === false) { ?>
        <div class="loginFeedback">
            <strong>Gebruikersnaam of wachtwoord is fout!</strong>
        </div>
    <?php } ?>
    <div class="loginContainer">
        <div id="login_div">
            <form action="<?php echo \ThemaC\MainController::SafePageName(); ?>" method="POST">
                <table>
                    <tr>
                        <td>
                            <h2>
                                Inloggen
                            </h2>
                            <label for="email">
                                Email:
                            </label><br>
                            <input type="email" name="email" id="email" placeholder="admin@glr.nl"/></td>
                    </tr>
                    <tr>
                        <td><label for="password">
                                Wachtwoord:
                            </label><br>
                            <input type="password" name="password" id="password" placeholder="Kees"/></td>
                    </tr>
                    <tr>
                        <td><input type="submit" class="submit" value="Inloggen"></td>
                    </tr>
                </table>
            </form>
        </div>
        <div id="login_div">
            <form action="<?php echo \ThemaC\MainController::SafePageName(); ?>" method="POST">
                <table>
                    <tr>
                        <td>
                            <h2>
                                Registeren
                            </h2>
                            <label for="code">
                                Studentnummer:
                            </label><br>
                            <input type="text" name="code" id="code" placeholder="69696"/></td>
                    </tr>
                    <tr>
                        <td><label for="firstName">
                                Voornaam:
                            </label><br>
                            <input type="text" name="firstName" id="firstName" placeholder="Kees"/></td>
                    </tr>
                    <tr>
                        <td><label for="lastName">
                                Achternaam:
                            </label><br>
                            <input type="text" name="lastName" id="lastName" placeholder="de Leeuw"/></td>
                    </tr>
                    <tr>
                        <td><label for="email">
                                Email:
                            </label><br>
                            <input type="email" name="email" id="email" placeholder="69696@glr.nl"/></td>
                    </tr>
                    <tr>
                        <td><label for="password">
                                Wachtwoord:
                            </label><br>
                            <input type="password" name="password" id="password"
                                   placeholder="CorrectHorseBatteryStaple"/>
                        </td>
                    </tr>
                    <tr>
                        <td><input type="submit" class="submit" value="Registeren"></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
</body>
</html>
