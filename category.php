<?php
/**
 * Created by PhpStorm.
 * User: 77711
 * Date: 14-1-2016
 * Time: 11:35
 */

require_once "inc/includes.php";
?>

    <div id="body">
        <div id="category">
            <div class="lastuploaded_videos">
                <?php
                if (isset($_GET['category']) && !is_numeric($_GET['category'])) {
                    $input = $_GET['category'];
                    $category = $categoryController->getCategoryByName($input);
                    $input = $category->getId();
                    $categories = [$category];
                } else {
                    if (isset($_GET['category'])) {
                        $input = $_GET['category'];
                        $categories = [$categoryController->getCategoryById($input)];
                    } else
                        $categories = $categoryController->getAllCategories();
                }

                foreach ($categories as $category) { ?>
                    <a class="category_title"
                       href="category.php?category=<?php echo $category->getId(); ?>"><?php echo $category->getName(); ?></a>
                    <div class="videoContainer">
                        <?php
                        $limit = (count($categories) === 1) ? "all" : 3;
                        $videos = $videoController->getVideosByCategory($category, $limit);
                        if (!empty($videos)) {
                            foreach ($videos as $video) {
                                if ($video instanceof \ThemaC\Video) {
                                    echo $video->getHtml($userController);
                                }
                            }
                        }
                        ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php require_once "inc/footer.php";
