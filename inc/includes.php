<?php
/**
 * Created by PhpStorm.
 * User: Wesley
 * Date: 7-1-2016
 * Time: 11:17
 */

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once "database.php";
require_once "getid3/getid3.php";
require_once "classes.php";
require_once "session.php";
require_once "header.php";
require_once "functions.php";


$mainController = new \ThemaC\MainController($dbh);
$videoController = new \ThemaC\VideoController($dbh);
$categoryController = new \ThemaC\CategoryController($dbh);
$userController = new \ThemaC\UserController($dbh);