<?php
/**
 * Created by PhpStorm.
 * User: Wesley Klop
 * Date: 13-1-2016
 * Time: 16:42
 */
ini_set('max_execution_time', 1200);
require_once "../inc/includes.php"; ?>
    <style>body {
            background-color: white;
        }</style><?php

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    echo "<pre>";
    $result = $videoController->uploadVideo($user, $_POST['name'], $_POST['description'], new \ThemaC\Category(1, "Lorem", "Ipsum"), $_FILES);
    var_dump($result);
    if ($result instanceof \ThemaC\Video) {
        echo "HET VIDEO UPLOADEN IS GEWOON GELUKT GEK";
        var_dump($result->getVideoThumbNail());
    }
    echo "</pre>";
    if ($result instanceof \ThemaC\Video) {
        echo "<img src={$result->getVideoThumbNail()} />";
        echo "<video width=\"640\" height=\"480\" controls><source src='/video.php?v={$result->getId()}' type='video/mp4'></video>";
    }
} else { ?>
    <form action="<?php echo \ThemaC\MainController::SafePageName(); ?>" method="POST" enctype="multipart/form-data">
        <label for="file">Video bestand:</label> <input type="file" name="video" id="file"/><br/>
        <label for="name">Video titel:</label> <input type="text" name="name" id="name"/><br/>
        <label for="description">Beschrijving:</label> <textarea name="description" id="description"></textarea>
        <input type="submit">
    </form>
<?php }
