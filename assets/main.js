/**
 * Created by Wesley on 21-1-2016.
 */
$(function () {
    var vid;
    if ((vid = $("video")).length) {
        "use strict";
        $("a").click(function () {
            "use strict";
            var source = vid.get(0);
            source.pause(0);
            source.src = "";
            source.load();
            vid.empty();
            source.remove();
        });
    }

    $(document).keydown(function (e) {
        "use strict";
        if (e.keyCode == 116) {
            var source = vid.get(0);
            if (typeof source != 'undefined') {
                source.pause(0);
                source.src = "";
                source.load();
                vid.empty();
                source.remove();
            }
        }
    })
});