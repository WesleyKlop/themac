<?php
/**
 * Created by PhpStorm.
 * User: 77711
 * Date: 12-1-2016
 * Time: 13:30
 */

require_once "inc/includes.php";
?>
<div id="lastuploaded">
    <div id="lastuploaded_title"><i>Laatst geupload</i></div>
    <!-- HALEN UIT DATABASE DIT DEEL-->
    <div class="lastuploaded_videos">
        <ul>
            <?php
            $videos = $videoController->getRecentVideos();
            if (!empty($videos)) {
                foreach ($videos as $video) {
                    if ($video instanceof \ThemaC\Video) {
                        echo $video->getHtml($userController, true);
                    }
                }
            }
            ?>
        </ul>
    </div>
</div>
<div id="body">
    <div id="content">
        <h1>Welkom op MediaTech videos</h1>
        <p>
            MediaTech is een video upload website gemaakt voor het Grafisch Lyceum Rotterdam.
            Door studenten van het Grafisch Lyceum Rotterdam. Op deze website kunnen docenten videos uploaden
            en de studenten kunnen hier dan inloggen met hun school account om deze videos bekijken.
        </p>

    </div>
    <div id="links">
        <div class="links">
            <img src="assets/GLR.png" alt="GLR">
            <a href="http://www.glr.nl">www.glr.nl</a>
        </div>
        <div class="links">
            <img src="assets/ICT-LAB.png" alt="ICT-LAB">
            <a href="http://www.ict-lab.nl">www.ict-lab.nl</a>
        </div>
    </div>
</div>
<?php require_once "inc/footer.php";