<?php
/**
 * Created by PhpStorm.
 * User: Wesley
 * Date: 14-1-2016
 * Time: 09:46
 */

require_once "../inc/includes.php";

if (isset($_GET['v'])) {
    $video = $videoController->getVideoById($_GET['v']);
    if ($video instanceof \ThemaC\Video) {
        echo "<pre>";
        var_dump($video);
        echo "</pre>";
        echo "<h1>{$video->getName()}</h1>";
        echo $video->getVideoWebPath();
        echo "<video controls><source src='/video.php?v={$video->getId()}' type='video/mp4' /></video>";
        echo "<p>{$video->getDescription()}</p>";
        echo "<img src='{$video->getVideoThumbNail()}' />";
    }
} else var_dump($_GET);