<?php
/**
 * Created by PhpStorm.
 * User: Wesley
 * Date: 7-1-2016
 * Time: 11:18
 */
?>
<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="icon" sizes="192x192" href=""/>
    <title>Mediatech video&apos;s | <?php echo ucfirst(basename($_SERVER['PHP_SELF'], ".php")); ?></title>
    <link href="https://fonts.googleapis.com/css?family=Material+Icons%7CRoboto" rel="stylesheet"/>
    <link rel="stylesheet" href="/assets/main.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="container">
    <div id="header">
        <img class="header_img" src="/assets/GLRLogo.png" alt="GLRLogo"><!-- dit ding staat altijd in de weg-->
        <ul>
            <li><a href="/home.php">Home</a></li>
            <li>
                <span><i class="material-icons">arrow_drop_down</i>Categorie</span>
                <ul>
                    <?php
                    $categoryController = new \ThemaC\CategoryController($dbh);
                    foreach ($categoryController->getAllCategories() as $category) { ?>
                        <li><a href="/category.php?category=<?php echo $category->getId(); ?>"><?php echo $category->getName(); ?></a></li>
                    <?php } ?>
                    <li><a href="/category.php">Meer categorieën...</a></li>
                </ul>
            </li>
            <?php
            if (isset($user) && $user instanceof \ThemaC\User) {
                if ($user->getUploadRights() === true) {
                    ?>
                    <li><a href="/dashboard.php">Dashboard</a></li>
                    <?php
                }
            } ?>
        </ul>
        <div id="header_inloggen">
            <div id="zoekbar">
                <div id="zoeken">
                    <form action="/search.php" method="GET">
                        <input type="text" id="search" name="search" placeholder="Zoeken...">
                        <input type="submit" id="submit" value="search" class="material-icons">
                    </form>
                </div>
            </div>
            <?php
            if (isset($user) && $user instanceof \ThemaC\User) {
                ?>
                <div id="ingelogd">
                    <div id="profilepic">
                        <?php
                        $gravatar = md5(strtolower(trim($user->getEmail())));
                        $grav_url = "http://www.gravatar.com/avatar/" . $gravatar . "?d=" . urlencode("http://video2.ict-lab.nl/assets/profilepic.png") . "&s=50";
                        ?>
                        <img class="profilepic" src="<?php echo $grav_url; ?>" alt="ProfilePicture">
                    </div>
                    <div id="ingelogd_user">

                        <?php
                        echo $user->getFirstName() . " " . $user->getLastName();
                        echo "<a href='/logout.php'>Uitloggen</a>";
                        ?>

                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>