<?php
/**
 * Created by PhpStorm.
 * User: Wesley
 * Date: 21-1-2016
 * Time: 09:19
 */
session_start();
session_destroy();

header("Location: /");
exit;