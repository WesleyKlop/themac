<?php
/**
 * Created by PhpStorm.
 * User: Wesley
 * Date: 11-2-2016
 * Time: 08:55
 */
require_once "inc/includes.php";

if (!isset($_GET['search']) || empty($_GET['search'])) {
    header("Location: home");
    exit;
}

$videos = $videoController->getVideosByName($_GET['search']);
// Of check of het een user is
$users = $userController->getUsersByName($_GET['search']);
if (is_array($users)) {
    foreach ($users as $user) {
        $userVideos = $videoController->getVideosByUser($user);
        if (!is_int($userVideos)) {
            foreach ($userVideos as $userVideo) {
                $videos[] = $userVideo;
            }
        }
    }
}

// Verwijder duplicaten
for ($i = 0; $i < count($videos); $i++) {
    $video = $videos[$i];
    for ($a = 0; $a < count($videos); $a++) {
        $dupVideo = $videos[$a];
        if ($video->getId() == $dupVideo->getId()
            && $a !== $i
        ) {
            //pre_dump("DUPLICATE VIDEO REMOVING KEY {$a} WITH ID {$dupVideo->getId()}");
            array_splice($videos, $a, 1);
        }
    }
}
?>
    <div id="body">
        <div id="category">
            <div class="lastuploaded_videos">
                <span class="category_title">Er zijn <?php echo count($videos); ?> resultaten gevonden</span>
                <div class="videoContainer">
                    <?php
                    foreach ($videos as $video) {
                        echo $video->getHtml($userController);
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php require_once "inc/footer.php";