<?php
/**
 * Created by PhpStorm.
 * User: 77711
 * Date: 14-1-2016
 * Time: 16:27
 */

$videoId = (isset($_GET['v'])) ? $_GET['v'] : 35;
require_once "inc/includes.php";

?>
<div id="videopreview_page">
    <div class="videopreview_page_video">
        <?php
        $video = $videoController->getVideoById($videoId);
        if ($video instanceof \ThemaC\Video) {
            // Check of de pagina een 2e request is, zo ja voeg dan geen extra view toe :D
            $referer = (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : "";

            if (!empty($referer) && !preg_match('/^http:\/\/video2\.ict-lab\.nl\/videopage\.php/', $referer)) {
                $videoController->addView($videoId);
            }
            echo "<h1 class='white'>{$video->getName()}</h1>"
                . "<video autoplay preload='metadata' width='1280px' height='720px' controls poster='{$video->getVideoThumbNail()}'>"
                . "<source src='/video.php?v={$video->getId()}' type='video/mp4' />"
                . "Je browser ondersteund geen video tags"
                . "</video>";
        }
        ?>
    </div>
</div>
<div id="body">
    <p>
        <?php echo $video->getDescription(); ?>
    </p>
</div>
<?php require_once "inc/footer.php";
