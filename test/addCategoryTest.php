<?php
/**
 * Created by PhpStorm.
 * User: Wesley
 * Date: 21-1-2016
 * Time: 09:52
 */

require_once "../inc/includes.php";

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    var_dump($categoryController->addCategory($_POST['name'], $_POST['description']));
} else { ?>
    <form action="<?php echo \ThemaC\MainController::SafePageName(); ?>" method="POST">
        <label for="name">Naam:</label> <input type="text" name="name" id="name"/><br/>
        <label for="description">Beschrijving:</label> <textarea name="description" id="description"></textarea>
        <input type="submit">
    </form>
<?php }
