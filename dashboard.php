<?php
/**
 * Created by PhpStorm.
 * User: 77711
 * Date: 21-1-2016
 * Time: 11:16
 */

require_once "inc/includes.php";

if ($_SERVER['REQUEST_METHOD'] === "POST") {
    if (isset($_POST['adduser']) && $_POST['adduser'] === "adduser") {
        $success = $userController->addUser($_POST['firstName'], $_POST['lastName'], $_POST['email'], $_POST['password'], $_POST['code']);
    }
    if (isset($_POST['uploadvideo']) && $_POST['uploadvideo'] === "uploadvideo") {
        // de $user is op deze pagina altijd een uploader
        $result = $videoController->uploadVideo($user, $_POST['name'], $_POST['description'], $_POST['category'], $_FILES);
    }
}
?>
    <div id="dashboard">
        <div id="information">
            <?php include_once "inc/dashboard.php"; ?>
        </div>
        <div id="dashboard_URL">
            <h1>User Controls</h1>
            <a href="http://video2.ict-lab.nl/dashboard.php?d=users">- Users</a><br/>
            <a href="http://video2.ict-lab.nl/dashboard.php?d=adduser">- Add user</a><br/>
            <a href="http://video2.ict-lab.nl/dashboard.php?d=changeuser">- Change user</a><br/>
            <h1>Video Controls</h1>
            <a href="http://video2.ict-lab.nl/dashboard.php?d=videos">- Video's</a><br/>
            <a href="http://video2.ict-lab.nl/dashboard.php?d=upload">- Upload video</a><br/>
            - Change video details<br/>
            <h1>Categories</h1>
            <a href="http://video2.ict-lab.nl/dashboard.php?d=categories">- Categories</a><br/>
            - Add category<br/>
            - Delete category<br/>
        </div>
    </div>

<?php
require_once "inc/footer.php";