<?php
/**
 * Created by PhpStorm.
 * User: Wesley
 * Date: 7-1-2016
 * Time: 11:19
 */
session_start();

if ((!isset($_SESSION['user'])
        || !($_SESSION['user'] instanceof \ThemaC\User))
    && \ThemaC\MainController::SafePageName() !== "/index.php"
) {
    header('location: /index');
    exit;
}

$user = (isset($_SESSION['user'])) ? $_SESSION['user'] : false;

if (($user instanceof \ThemaC\User)
    && (\ThemaC\MainController::SafePageName() === "/dashboard.php"
        && $user->getUploadRights() === false)
) {
    header("location: /home");
    exit;
}